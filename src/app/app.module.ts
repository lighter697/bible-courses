import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ChapterComponent} from './components/chapter/chapter.component';
import {SummaryComponent} from './components/summary/summary.component';
import {ChapterModule} from "./components/chapter/chapter.module";
import {ParagraphComponent} from './components/paragraph/paragraph.component';
import {HttpClientModule} from "@angular/common/http";
import {DefaultDataServiceConfig, EntityDataModule, EntityMetadataMap} from "@ngrx/data";
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {ChapterModel} from "./models/chapter.model";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AnswerModel} from "./models/answer.model";
import {environment} from "../environments/environment";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import { ResultsComponent } from './components/results/results.component';

export function selectChapterId(event: ChapterModel): string {
  return event.uuid;
}

export function selectAnswerId(event: AnswerModel): string {
  return event.paragraphUUid;
}

const entityMetadata: EntityMetadataMap = {
  chapter: {
    selectId: selectChapterId
  },
  answer: {
    selectId: selectAnswerId
  }
};

const pluralNames = {chapter: 'chapters', answer: 'answers'};

export const entityConfig = {
  entityMetadata,
  pluralNames
};

const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: '/assets/',
  entityHttpResourceUrls: {
    chapter: {
      entityResourceUrl: null,
      collectionResourceUrl: '/assets/chapters.json'
    }
  },
  timeout: 3000, // request timeout
}

@NgModule({
  declarations: [
    AppComponent,
    ChapterComponent,
    SummaryComponent,
    ParagraphComponent,
    ResultsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChapterModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(entityConfig),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {ChoiceModel} from "./choice.model";

export class QuestionModel {
  uuid: string;
  question: string;
  type: string;
  choices: ChoiceModel[];
}

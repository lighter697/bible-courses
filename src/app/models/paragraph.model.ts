import {ChoiceModel} from "./choice.model";

export class ParagraphModel {
  uuid: string;
  text: string;
  question: string;
  type: string;
  choices: ChoiceModel[];
}

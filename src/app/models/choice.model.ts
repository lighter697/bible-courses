export class ChoiceModel {
  uuid: string;
  title: string;
  correct: boolean;
  selected: boolean;
}

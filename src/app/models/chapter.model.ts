import {ParagraphModel} from "./paragraph.model";

export class ChapterModel {
  uuid: string;
  title: string;
  paragraphs: ParagraphModel[];
  summary: any[];
}

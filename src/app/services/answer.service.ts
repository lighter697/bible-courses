import {Injectable} from '@angular/core';
import {EntityCollectionServiceBase, EntityCollectionServiceElementsFactory} from "@ngrx/data";
import {HttpClient} from "@angular/common/http";
import {AnswerModel} from "../models/answer.model";

@Injectable({
  providedIn: 'root'
})
export class AnswerService extends EntityCollectionServiceBase<AnswerModel> {

  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory,
    private http: HttpClient,
  ) {
    super('answer', serviceElementsFactory);
  }

  // lalal(chapterUuid): Observable<ChapterModel> {
  //   return this.http.get<ChapterModel>(`/assets/chapters/${chapterUuid}.json`);
  // }

  // getAll(options?: EntityActionOptions): Observable<ChapterModel[]> {
  //   return this.http.get<ChapterModel[]>('/assets/chapters.json');
  // }
}

import {Injectable} from '@angular/core';
import {ChapterModel} from "../models/chapter.model";
import {EntityActionOptions, EntityCollectionServiceBase, EntityCollectionServiceElementsFactory} from "@ngrx/data";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ChapterService extends EntityCollectionServiceBase<ChapterModel> {

  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory,
    private http: HttpClient,
  ) {
    super('chapter', serviceElementsFactory);
  }

  // lalal(chapterUuid): Observable<ChapterModel> {
  //   return this.http.get<ChapterModel>(`/assets/chapters/${chapterUuid}.json`);
  // }

  // getAll(options?: EntityActionOptions): Observable<ChapterModel[]> {
  //   return this.http.get<ChapterModel[]>('/assets/chapters.json');
  // }
}

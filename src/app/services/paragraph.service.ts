import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ChapterModel} from "../models/chapter.model";
import {ParagraphModel} from "../models/paragraph.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ParagraphService {

  constructor(private http: HttpClient) {
  }

  get(chapterUuid, paragraphUuid): Observable<ParagraphModel> {
    return this.http.get<ChapterModel>(`/assets/chapters/${chapterUuid}.json`).pipe(
      map((chapter: ChapterModel) => chapter.paragraphs.find((p: ParagraphModel) => p.uuid === paragraphUuid))
    );
  }
}

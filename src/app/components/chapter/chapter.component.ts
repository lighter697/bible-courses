import {Component, OnInit} from '@angular/core';
import {map, switchMap} from "rxjs/operators";
import {Observable} from "rxjs";
import {ChapterModel} from "../../models/chapter.model";
import {ChapterService} from "../../services/chapter.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.scss']
})
export class ChapterComponent implements OnInit {
  chapter$: Observable<ChapterModel>;


  constructor(
    private chapterService: ChapterService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.chapterService.load();

    this.chapter$ = this.route.params.pipe(
      switchMap((params) => this.chapterService.entities$.pipe(
        map((chapters: ChapterModel[]) => chapters.find((chapter: ChapterModel) => chapter.uuid === params.uuid))
      )),
    );
  }

}
